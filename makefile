all: process

process:
	gcc -D PROCESS server.c -pthread -o server
	gcc client.c -o client

thread:
	gcc -D THREAD server.c -pthread -o server
	gcc client.c -o client
clean:
	rm -f client
	rm -f server