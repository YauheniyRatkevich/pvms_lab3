#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>
#include <unistd.h>

char buf[1024];

int main(int argc, char *argv[]) {
    char *ip_address = argv[1];
    char *message = argv[2];
    int sock;
    int read_bytes;
    struct sockaddr_in addr;
    struct hostent *server;

    sock = socket(AF_INET, SOCK_STREAM, 0);
    if (sock < 0) {
        perror("socket");
        return 0;
    }

    server = gethostbyname(ip_address);
    if (server == NULL) {
        perror("no such server");
        return 0;
    }

    bzero((char *) &addr, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(3425); // или любой другой порт...
    bcopy(server->h_addr,
          (char *) &addr.sin_addr.s_addr,
          server->h_length);
    if (connect(sock, (struct sockaddr *)&addr, sizeof(addr)) < 0) {
        perror("connect");
        return 0;
    }

    write(sock, message, strlen(message));

    while (1) {
        read_bytes = recv(sock, buf, sizeof(buf), 0);
        if (read_bytes <= 0) break;
        printf("%s", buf);
    }

    close(sock);

    return 0;
}