#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <string.h>
#include <pthread.h>

void *connection_function(void *);

int main(int argc, char *argv[]) {
    int sock, listener, pid, clilen;
    struct sockaddr_in addr, cli_addr;
    char buf[1024];
    int bytes_read;

    listener = socket(AF_INET, SOCK_STREAM, 0);

    if (listener < 0) {
        perror("socket");
        return 0;
    }

    memset((char *) &addr, 0, sizeof(addr));
    addr.sin_family = AF_INET;
    addr.sin_port = htons(3425);
    addr.sin_addr.s_addr = INADDR_ANY;
    if (bind(listener, (struct sockaddr *) &addr, sizeof(addr)) < 0) {
        perror("bind");
        return 0;
    }

    listen(listener, 3);

    clilen = sizeof(cli_addr);

    while ((sock = accept(listener, (struct sockaddr *) &cli_addr, &clilen))) {

        #ifdef THREAD
            pthread_t thread_id;
            if (pthread_create(&thread_id, NULL, connection_function, (void *) &sock) < 0) {
                perror("could not create thread");
                return 0;
            }
            pthread_join(thread_id, NULL);
        #endif

        #ifdef PROCESS
            pid = fork();
            if (pid < 0) {
                perror("fork error");
                return 0;
            }
            if (pid == 0) {
            close(listener);
            connection_function((void *) &sock);
            return 0;
            }
            else close(sock);
        #endif
    }
    close(sock);

    shutdown(listener, 2);
    return 0;
}

void *connection_function(void *socket_desc) {
    int socket = *(int *) socket_desc;
    int bytes_read;
    char buffer[1024];
    while (bytes_read = read(socket, buffer, 1023) > 0) {
        FILE *fd;
        int status;
        char path[1024];
        fd = popen(buffer, "r");
        if (fd == NULL) {
            perror("file error");
            return 0;
        }
        while (fgets(path, 1024, fd) != NULL) {
            send(socket, path, 1024, 0);
        }
        shutdown(socket, 1);
        status = pclose(fd);
        if (status == -1) {
            perror("damn it");
            return 0;
        }

    }

    pthread_exit(NULL);
}